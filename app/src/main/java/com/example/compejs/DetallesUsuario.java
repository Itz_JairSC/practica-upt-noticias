package com.example.compejs;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class DetallesUsuario extends AppCompatActivity {
    public TextView txtUsername, txtPassword;
    public Button btnVolver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_usuario);

        txtUsername = (TextView) findViewById(R.id.txtUsername);
        txtPassword = (TextView) findViewById(R.id.txtPassword);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            String mensaje = bundle.getString("texto1");
            String mensaje2 = bundle.getString("texto2");
            txtUsername.setText(mensaje);
            txtPassword.setText(mensaje2);
        }
        else{
            Toast.makeText(DetallesUsuario.this, "No se puede mostrar nada", Toast.LENGTH_SHORT).show();
        }

btnVolver = (Button) findViewById(R.id.btnRegresar);
        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DetallesUsuario.this,Inicio.class);
                startActivity(intent);
                Toast.makeText(DetallesUsuario.this, "De vuelta al inicio", Toast.LENGTH_SHORT).show();
            }
        });



    }
}
