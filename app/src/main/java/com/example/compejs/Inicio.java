package com.example.compejs;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Inicio extends AppCompatActivity {
    TextView txtTitulo, txtDescripcion, txtSubido, txtActualizacion;
    Button btnMostrar;
    Button btnCerrarS;
    Button btnNotificacion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
        txtTitulo = (TextView) findViewById(R.id.txtTitulo);
        txtDescripcion = (TextView) findViewById(R.id.txtDescripcion);
        txtSubido = (TextView) findViewById(R.id.txtSubido);
        txtActualizacion = (TextView) findViewById(R.id.txtActualizado);
        btnMostrar = (Button) findViewById(R.id.MostrarUsers);
        btnMostrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ServicioPeticion service = Api.getApi(Inicio.this).create(ServicioPeticion.class);
                Call<Mostrar_Noticias> mostrarinfo = service.getNoticias();
                mostrarinfo.enqueue(new Callback<Mostrar_Noticias>() {
                    @Override
                    public void onResponse(Call<Mostrar_Noticias> call, Response<Mostrar_Noticias> response) {
                        Mostrar_Noticias peticion = response.body();
                        if(response.body()== null){
                            Toast.makeText(Inicio.this, "Ocurrió un error, intentalo más tarde", Toast.LENGTH_LONG).show();
                            return;
                        }

                        if(peticion.estado =="true"){

                        txtTitulo.setText(peticion.detalle.get(0).titulo);
                        txtDescripcion.setText(peticion.detalle.get(0).descripcion);
                        txtSubido.setText(peticion.detalle.get(0).created_at);
                        txtActualizacion.setText(peticion.detalle.get(0).updated_at);
                        Toast.makeText(Inicio.this,"Datos cargados correctamente", Toast.LENGTH_LONG).show();


                        }
                        else {
                            Toast.makeText(Inicio.this, "Error del servidor", Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<Mostrar_Noticias> call, Throwable t) {
                        Toast.makeText(Inicio.this, "Error inesperado", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });
        btnCerrarS = (Button) findViewById(R.id.btnCerrarS);
        btnCerrarS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferencias.edit();
                editor.putString("TOKEN","");
                editor.commit();
                Toast.makeText(Inicio.this, "Ha serrado sesión satisfactoriamente", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Inicio.this,MainActivity.class);
                startActivity(intent);
            }
        });

        btnNotificacion = (Button) findViewById(R.id.btnCerrarS2);
        btnNotificacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Inicio.this,Notificaciones.class);
                startActivity(intent);
            }
        });

    }


}
