package com.example.compejs;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
Button BtnInicio;
Button BtnRegistro;
private String APITOKEN;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN","");
        if(token != ""){
            Toast.makeText(MainActivity.this,"Bienvenido de vuelta",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(MainActivity.this,Inicio.class);
            startActivity(intent);
        }
        final EditText correo = findViewById(R.id.ETcorreo);
        final EditText contraseña = findViewById(R.id.ETpassword);


        BtnInicio = (Button) findViewById(R.id.Iniciar);
        BtnInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (correo.getText().toString().isEmpty() || correo.getText().toString() == ""){
                    correo.setSelectAllOnFocus(true);
                    correo.requestFocus();
                    Toast.makeText(MainActivity.this,"Rellena los campos para logearte!",Toast.LENGTH_LONG).show();
                    return;
                }

                if (contraseña.getText().toString().isEmpty() || contraseña.getText().toString() == ""){
                    contraseña.setSelectAllOnFocus(true);
                    contraseña.requestFocus();
                    Toast.makeText(MainActivity.this,"Rellena los campos para logearte!",Toast.LENGTH_LONG).show();
                    return;
                }

                ServicioPeticion service = Api.getApi(MainActivity.this).create(ServicioPeticion.class);
                Call<Peticion_Login> loginCall = service.getlogin(correo.getText().toString(),contraseña.getText().toString());
                loginCall.enqueue(new Callback<Peticion_Login>() {
                    @Override
                    public void onResponse(Call<Peticion_Login> call, Response<Peticion_Login> response) {
                        Peticion_Login peticion = response.body();
                        if(peticion.estado == "true"){
                            APITOKEN = peticion.token;
                            GuardarPreferencias();
                            Toast.makeText(MainActivity.this,"Bienvenido al servicio",Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(MainActivity.this,Inicio.class);
                            startActivity(intent);
                        }
                        else{

                            Toast.makeText(MainActivity.this,"Verifica tus datos",Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Peticion_Login> call, Throwable t) {
                        Toast.makeText(MainActivity.this, "Error inesperado", Toast.LENGTH_LONG).show();
                        return;
                    }
                });


            }
        });

        BtnRegistro = (Button) findViewById(R.id.Registro);
        BtnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Registro.class);
                startActivity(intent);
            }
        });
    }

    public void GuardarPreferencias(){
        SharedPreferences preferencias = getSharedPreferences("credenciales",Context.MODE_PRIVATE);
        String token = APITOKEN;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("TOKEN",token);
        editor.commit();
    }

    public void CargarPreferencias(){
        SharedPreferences preferencias = getSharedPreferences("credenciales",Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN","Haber que sale");

    }
}
