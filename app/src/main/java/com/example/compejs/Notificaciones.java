package com.example.compejs;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.ReferenceQueue;
import java.util.HashMap;
import java.util.Map;

public class Notificaciones extends AppCompatActivity {
Button Especifico, General;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notificaciones);
        Especifico = (Button) findViewById(R.id.btnEspecifico);
        General = (Button) findViewById(R.id.btnGeneral);

        FirebaseMessaging.getInstance().subscribeToTopic("atodos").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(Notificaciones.this,"Se agregó a la lista",Toast.LENGTH_LONG).show();
            }
        });

        Especifico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            llamarespecifico();
            }
        });

        General.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            llamargeneral();
            }
        });

    }

    public void llamarespecifico(){
        RequestQueue myrequuest = Volley.newRequestQueue(getApplicationContext());
        JSONObject json = new JSONObject();
        try{
            String token = "cMtCVD7KWAc:APA91bGP5TeGttUL4LsEgTkkdVllN3ATncmLWeQ620mkSr7sepUJBhOXnTxQQHSFcJ9Ao26gZxD05z_MNMgweIcH-JwZIjDHfIcjWoN50RfnGharpRnXhRZOxSb7TPdsI_F-xAm27XeF";
            json.put("to",token);
            JSONObject notificacion = new JSONObject();
            notificacion.put("titulo", "Mostrando titulo");
            notificacion.put("detalle","mostrando contenido");
            json.put("data", notificacion);
            String URL = "https://fcm.googleapis.com/fcm/send";

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,URL,json,null,null){
                @Override
                public Map<String, String> getHeaders()  {
                    Map<String,String> header = new HashMap<>();
                    header.put("content-type","application/json");
                    header.put("authorization","key=AAAAOAJT3Hk:APA91bGUYem1zAwmuXlo_kecPyfErc8M3-X8gj_XcFclyhTQ14vbn2eredQtJaBz2qNZtsOT3jkhyFErd-V7MIEyA-gOlNMYMktVLfYhRqc8z6GNvXenhoh-iyCCGjaUkIcGMVty3ayt");
                    return header;
                }
            };
            myrequuest.add((request));

        }catch (JSONException e){
            e.printStackTrace();
        }


    }

    public void llamargeneral(){
        RequestQueue myrequuest = Volley.newRequestQueue(getApplicationContext());
        JSONObject json = new JSONObject();
        try{
            //String token = "cMtCVD7KWAc:APA91bGP5TeGttUL4LsEgTkkdVllN3ATncmLWeQ620mkSr7sepUJBhOXnTxQQHSFcJ9Ao26gZxD05z_MNMgweIcH-JwZIjDHfIcjWoN50RfnGharpRnXhRZOxSb7TPdsI_F-xAm27XeF";
            json.put("to","/topics/"+"atodos");
            JSONObject notificacion = new JSONObject();
            notificacion.put("titulo", "Mostrando titulo");
            notificacion.put("detalle","mostrando contenido");
            json.put("data", notificacion);
            String URL = "https://fcm.googleapis.com/fcm/send";

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,URL,json,null,null){
                @Override
                public Map<String, String> getHeaders()  {
                    Map<String,String> header = new HashMap<>();
                    header.put("content-type","application/json");
                    header.put("authorization","key=AAAAOAJT3Hk:APA91bGUYem1zAwmuXlo_kecPyfErc8M3-X8gj_XcFclyhTQ14vbn2eredQtJaBz2qNZtsOT3jkhyFErd-V7MIEyA-gOlNMYMktVLfYhRqc8z6GNvXenhoh-iyCCGjaUkIcGMVty3ayt");
                    return header;
                }
            };
            myrequuest.add((request));

        }catch (JSONException e){
            e.printStackTrace();
        }


    }
}