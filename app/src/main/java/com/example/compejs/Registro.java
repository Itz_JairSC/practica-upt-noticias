package com.example.compejs;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registro extends AppCompatActivity {
Button btnRegistrar;
Button BtnVolver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        btnRegistrar = (Button) findViewById(R.id.Registrar);
        btnRegistrar.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        EditText correo = (EditText) findViewById(R.id.Username);
        EditText contrasenia = (EditText) findViewById(R.id.Contrasenia);
        EditText confirmarC = (EditText) findViewById(R.id.ConfirmarC);

        if (correo.getText().toString() == "" || correo.getText().toString().isEmpty()) {
            correo.setSelectAllOnFocus(true);
            correo.requestFocus();
            Toast.makeText(Registro.this, "Inserta un nombre de usuario", Toast.LENGTH_LONG).show();
            return;
        }
        if (contrasenia.getText().toString() == ""|| contrasenia.getText().toString().isEmpty()) {
            contrasenia.setSelectAllOnFocus(true);
            contrasenia.requestFocus();
            Toast.makeText(Registro.this, "Ingresa una contraseña", Toast.LENGTH_LONG).show();
            return;
        }
        if (confirmarC.getText().toString() == ""|| confirmarC.getText().toString().isEmpty()) {
            confirmarC.setSelectAllOnFocus(true);
            confirmarC.requestFocus();
            Toast.makeText(Registro.this, "Ingresa de nuevo tu contraseña", Toast.LENGTH_LONG).show();
            return;
        }

        if (!contrasenia.getText().toString().equals(confirmarC.getText().toString())){
            Toast.makeText(Registro.this, "Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
            return;
        }

        ServicioPeticion service = Api.getApi(Registro.this).create(ServicioPeticion.class);
        Call<Registro_Usuario> registrarcall = service.registrarUsuario(correo.getText().toString(),contrasenia.getText().toString());
        registrarcall.enqueue(new Callback<Registro_Usuario>() {
    @Override
    public void onResponse(Call<Registro_Usuario> call, Response<Registro_Usuario> response) {
        Registro_Usuario peticion = response.body();
        if(response.body()== null){
            Toast.makeText(Registro.this, "Ocurrió un error, intentalo más tarde", Toast.LENGTH_LONG).show();
            return;
        }

        if (peticion.estado == "true"){
            Toast.makeText(Registro.this, "Datos Registrados Corrrectamente", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(Registro.this, MainActivity.class);
            startActivity(intent);

        }
        else {
            Toast.makeText(Registro.this, peticion.detalle, Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onFailure(Call<Registro_Usuario> call, Throwable t) {
Toast.makeText(Registro.this, "Error inesperado", Toast.LENGTH_LONG).show();
return;
    }
});
        //Intent intent = new Intent(Registro.this, MainActivity.class);
        //startActivity(intent);
    }
});

BtnVolver = (Button) findViewById(R.id.Volver);
BtnVolver.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent = new Intent(Registro.this, MainActivity.class);
        startActivity(intent);
    }
});
    }
}
