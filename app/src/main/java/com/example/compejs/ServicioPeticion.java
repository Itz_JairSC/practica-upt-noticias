package com.example.compejs;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ServicioPeticion {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Registro_Usuario> registrarUsuario (@Field("username") String correo , @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/loginSocial")
    Call<Peticion_Login> getlogin(@Field("username") String correo, @Field("password") String contraseña);

    @GET("api/todasNot")
    Call<Mostrar_Noticias> getNoticias();

    @POST("api/todosUsuarios")
    Call<Mostrar_Usuarios> getUsuarios();

    @FormUrlEncoded
    @POST("api/detallesUsuario")
    Call<User_Detalle> getDetalles(@Field("usuarioId") int usuarioId);


}
